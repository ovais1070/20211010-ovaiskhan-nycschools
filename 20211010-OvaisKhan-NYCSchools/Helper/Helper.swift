//
//  Helper.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation
import UIKit

typealias VoidBlock = () -> ()
typealias ResponseBlock = (RequestStatus, String) -> ()
