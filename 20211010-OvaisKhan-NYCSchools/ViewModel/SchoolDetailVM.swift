//
//  SchoolDetailVM.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/11/21.
//

import Foundation

class SchoolDetailVM: NSObject {
    
    var data: School_List_Model?
    var satScores: SAT_Score_Model?
    var notFount: Bool?
    var lang: Double?
    var lat: Double?
    
 }

