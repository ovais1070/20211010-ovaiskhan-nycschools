//
//  SchoolListVM.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation
import UIKit



class SchoolListVM: NSObject {
    
    fileprivate let service = NetworkDataServices()
    var message: String?
    var schoolList: [School_List_Model]?
    var satScores: [SAT_Score_Model]?
    var filteredschools = [School_List_Model]()
    var postSatScore: SAT_Score_Model?
    
    
    
    func getSchoolList(completion: @escaping ((ViewModelState) -> Void)) {
        service.request_School_List { data, error, request_error  in
            if let error = error {
                self.message = error.localizedDescription
                print("done: fetching \(error)")
                completion(.failure)
                return
            } else if  request_error != nil {
                self.message = request_error?.message ?? "smth went wrong"
                completion(.failure)
                return
            }
            
            self.schoolList = data
            completion(.success)
        }
    }
    

    func getScore(completion: @escaping ((ViewModelState) -> Void)) {
        service.request_SAT_score { data, error, request_error  in
            if let error = error {
                self.message = error.localizedDescription
                print("done: fetching \(error)")
                completion(.failure)
                return
            } else if  request_error != nil {
                self.message = request_error?.message ?? "smth went wrong"
                completion(.failure)
                return
            }
            
            
            self.satScores = data
            
            completion(.success)
        }
    }
    
    
}

