//
//  NetworkDataServices.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation
import Moya
import Alamofire

class NetworkDataServices {
    
    private let provider = MoyaProvider<NetworkServices>(plugins: [NetworkLoggerPlugin()])
    
    func request_School_List(completion: @escaping (([School_List_Model]?, Error?, ErrorHandler?) -> Void)) {
        provider.request(.getSchools) { result in
            switch result {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([School_List_Model]?.self, from: response.data)
                    completion(data, nil, nil)
                } catch (let error) {
                    completion(nil, error, nil)
                }
            case .failure(let error):
                completion(nil, error,nil)
            }
        }
    }
    
    
    func request_SAT_score(completion: @escaping (([SAT_Score_Model]?, Error?, ErrorHandler?) -> Void)) {
        provider.request(.getSatScores) { result in
            switch result {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([SAT_Score_Model]?.self, from: response.data)
                    completion(data, nil, nil)
                } catch (let error) {
                    completion(nil, error, nil)
                }
            case .failure(let error):
                completion(nil, error,nil)
            }
        }
    }
    
}

