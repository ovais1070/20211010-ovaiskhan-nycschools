//
//  NetworkServices.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/11/21.
//

import Foundation
import Moya
import Foundation

enum RequestStatus {
    case ok
    case error
    case fail
}


typealias Params = [String:Any]

enum NetworkServices {
   
    case getSchools
    case getSatScores
    
}

extension NetworkServices: TargetType {
    
    static let provider = MoyaProvider<NetworkServices>(plugins: [NetworkLoggerPlugin()])
    
    var baseURL: URL {
        
        let baseUrl = "https://data.cityofnewyork.us/resource/"// PROD

        guard let url = URL(string: baseUrl) else {
            fatalError("URL cannot be configured.")
        }
        return url
    }
    
    var path: String {
        switch self {
        
       
            
        case .getSchools: return "s3k6-pzi2.json"
        case .getSatScores: return "f9bf-2cp4.json"
        }
    }
    
    var method: Moya.Method {
        switch self {
        
        case .getSchools: return .get
        case .getSatScores: return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        
        default:
            return .requestPlain
        }
    }
    
    
    var headers: [String : String]? {
        
        switch self {
       
        default: return nil
            
            
        }
       
        
      
    }

}


