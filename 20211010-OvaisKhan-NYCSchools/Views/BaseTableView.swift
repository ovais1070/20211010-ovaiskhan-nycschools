//
//  BaseTableView.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation
import UIKit

class BaseTableView: UITableView {
    
    var onRefreshHandler: VoidBlock?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLoader()
    }
    
    private func setupLoader() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        
        // this is the replacement of implementing: "collectionView.addSubview(refreshControl)"
        self.refreshControl = refreshControl
    }
    
    @objc private func refreshTableView() {
        
        onRefreshHandler?()
    }
    
    func startRefresh() {
        self.refreshControl?.beginRefreshing()
        
        let contentOffset = CGPoint(x: 0, y: -(refreshControl?.frame.height)!)
        setContentOffset(contentOffset, animated: true)
    }
    
    func endRefresh() {
        self.refreshControl?.endRefreshing()
    }
    
    override func reloadData() {
        super.reloadData()
        
        self.refreshControl?.endRefreshing()
    }
    
    override func reloadSections(_ sections: IndexSet, with animation: UITableView.RowAnimation) {
        super.reloadSections(sections, with: animation)
        
        self.refreshControl?.endRefreshing()
    }
    
    
}
