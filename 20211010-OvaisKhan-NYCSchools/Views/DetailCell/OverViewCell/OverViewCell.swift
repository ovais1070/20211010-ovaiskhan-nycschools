//
//  OverViewCell.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import UIKit

class OverViewCell: UITableViewCell {

    
    @IBOutlet weak var overViewDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func configureCell(data: School_List_Model){
        
        overViewDescription.text = data.overview_paragraph
    }
}
