//
//  ScoreCell.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import UIKit
import Foundation
import Cartography

class ScoreCell: UITableViewCell {

    
    
    @IBOutlet weak var readingSatScore: UILabel!
    @IBOutlet weak var mathSatScore: UILabel!
    @IBOutlet weak var writingSatScore: UILabel!
    @IBOutlet weak var satScoreStack: UIStackView!
    @IBOutlet weak var main_Vu: UIView!
    @IBOutlet weak var main_vu_Height: NSLayoutConstraint!
    
    var emptyListPlaceholder: UIView!
    var emptyListLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    
    func configureCell(scores: SAT_Score_Model) {
        
        main_vu_Height.constant = 90
        self.readingSatScore.text = "Average Critical Reading SAT Score: " + scores.sat_critical_reading_avg_score!
        self.mathSatScore.text = "Average Math SAT Score" + scores.sat_math_avg_score!
        self.writingSatScore.text = "Average Writing SAT Score" + scores.sat_writing_avg_score!
        
    }
    
    
    func showEmptyListPlaceholder(title: String = "No SAT Score Found") {
        self.emptyListPlaceholder.isHidden = false
        self.emptyListLabel.text = title
       
    }
    
    func hideEmptyListPlaceholder() {
        emptyListPlaceholder.isHidden = true
    }
    
    
    private func setupViews() {
        emptyListPlaceholder = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 180))
        
        main_vu_Height.constant = 100
        emptyListLabel = UILabel()
        emptyListLabel.textColor = .black
        emptyListLabel.font = UIFont(name: "ProximaNova-Bold", size: 14)
        emptyListLabel.textAlignment = .center
        emptyListLabel.numberOfLines = 0
        emptyListPlaceholder.addSubview(emptyListLabel)
        constrain(emptyListLabel, emptyListPlaceholder) { label, view in
            label.width == 200
            label.centerX == view.centerX
            
        }
        
        
        
        self.contentView.addSubview(emptyListPlaceholder)
        constrain(emptyListPlaceholder, self.contentView) { placeholder, view in
            placeholder.top == view.top + 30
            placeholder.leading == view.leading + 16
            placeholder.trailing == view.trailing + 16
            placeholder.bottom == view.bottom + 10
        }
        
        emptyListPlaceholder.isHidden = true
        
    }
    
}
