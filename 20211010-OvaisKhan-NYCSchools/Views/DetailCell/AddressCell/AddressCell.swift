//
//  AddressCell.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import UIKit
import MapKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navigateBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell (data: School_List_Model){
        phone.text  = "Phone Number: \(data.phone_number!)"
        address.text = "Address: " + data.primary_address_line_1! + " " + data.zip! + " " + data.city! + " " + data.state_code!
        website.text = "Website: " + data.website!
        
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(data.latitude!)!), longitude: CLLocationDegrees(Double(data.longitude!)!))
        addHSAnnotaionWithCoordinates(coordinates)
    }
    
    
    
    func addHSAnnotaionWithCoordinates(_ schoolCoordinates: CLLocationCoordinate2D){
        
        let schoolAnnotation = MKPointAnnotation()
        schoolAnnotation.coordinate = schoolCoordinates
        self.mapView.addAnnotation(schoolAnnotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        let region = MKCoordinateRegion(center: schoolAnnotation.coordinate, span: span)
        let adjustRegion = self.mapView.regionThatFits(region)
        self.mapView.setRegion(adjustRegion, animated:true)
    }
}
