//
//  ListTableCell.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import UIKit

class ListTableCell: UITableViewCell {

    
    //Outlets
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var card_Vu: UIView!
    @IBOutlet weak var mobile_number: UIButton!
    @IBOutlet weak var address: UILabel!
    
    
    // varibales and constants
    var data: School_List_Model?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        chooseRandomColorForCard()
        card_Vu.drawShadow(forOpacity: 0.1, forOffset: .zero)
        card_Vu.layer.cornerRadius = 10
        mobile_number.layer.cornerRadius = 10
        mobile_number.layer.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configure(data: School_List_Model) {
        
        schoolName.text = data.school_name
        self.data = data
        mobile_number.setTitle("Phone Number: \(data.phone_number!)", for:.normal)
        address.text = data.primary_address_line_1! + " " + data.zip! + " " + data.city! + " " + data.state_code!
    }
    
    func chooseRandomColorForCard(){
        
        self.card_Vu .layer.borderWidth = 1
        self.card_Vu.layer.borderColor = UIColor.randomColorForCardViewCell().cgColor
    }
    
    
    
    
    @IBAction func mobile_number(_ sender: Any) {
        
        callNumber(phoneNumber: (self.data?.phone_number)!)
    }
    
    
    private func callNumber(phoneNumber: String) {
        guard let url = URL(string: "telprompt://\(phoneNumber)"),
            UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
