//
//  Extensions.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation
import UIKit


extension UIView {
    

    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    
    func roundedBottomcorners(radius:CGFloat){
       clipsToBounds = true
       layer.cornerRadius = radius
       layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    func drawShadow(color: CGColor = UIColor.black.cgColor,
                    forOpacity opacity: Float,
                    forOffset offset: CGSize,
                    radius: CGFloat = 10) {
        layer.masksToBounds = false
        layer.shadowColor = color
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat, borderColor: UIColor?, borderWidth: CGFloat?) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        
        let mask = CAShapeLayer()
        mask.frame = self.bounds
        mask.path = path.cgPath
        self.layer.mask = mask
        
        if borderWidth != nil {
            addBorder(mask, borderWidth: borderWidth!, borderColor: borderColor!)
        }
    }
    
    private func addBorder(_ mask: CAShapeLayer, borderWidth: CGFloat, borderColor: UIColor) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
}

extension UIColor {
    
    class func randomColorForCardViewCell() -> UIColor {
        let lightRed: UIColor? = UIColor(red: 0.925, green: 0.290, blue: 0.255, alpha: 1.00)
        let lightBlue: UIColor? = UIColor(red: 0.000, green: 0.639, blue: 0.812, alpha: 1.00)
        let lightGray: UIColor? = UIColor(red: 0.773, green: 0.773, blue: 0.773, alpha: 1.00)
        let indigo: UIColor? = UIColor(red: 0.361, green: 0.420, blue: 0.753, alpha: 1.00)
        let green: UIColor? = UIColor(red: 0.298, green: 0.686, blue: 0.314, alpha: 1.00)
        let yellow: UIColor? = UIColor(red: 1.000, green: 0.933, blue: 0.345, alpha: 1.00)
        let deepOrange: UIColor? = UIColor(red: 1.000, green: 0.439, blue: 0.263, alpha: 1.00)
        
        let sideBarColorArr: [UIColor] = [lightRed!, lightBlue!, lightGray!, indigo!, green!, yellow!, deepOrange!]
        
        let randomNumber = arc4random_uniform(UInt32(sideBarColorArr.count))
        
        return sideBarColorArr[Int(randomNumber)]
    }
    
}

