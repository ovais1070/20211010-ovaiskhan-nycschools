//
//  Handlers.swift
//  20211010-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/10/21.
//

import Foundation


enum ViewModelState {
    case success
    case failure
}

struct ErrorHandler:Codable {
    let code:Int?
    let message:String?
}

