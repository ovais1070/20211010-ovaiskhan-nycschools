//
//  ViewController.swift
//  20211002-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/2/21.
//

import UIKit


class SplashVC: UIViewController {
    
    
    //Outlets
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var vu_bg: UIView!
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UIAnimation for the Splash Screen
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        vu_bg.layer.cornerRadius = vu_bg.frame.size.width/2
        vu_bg.clipsToBounds = true
        
        UIView.animate(withDuration: 4,
                       animations: {
            self.vu_bg.transform = CGAffineTransform(scaleX: 100, y: 100)
        },
                       completion: { _ in
            
            DispatchQueue.main.async {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchoolListVC") as! SchoolListVC
                let navigationController = UINavigationController(rootViewController: vc)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
                
                
                
                
            }
            
        })
        
    }
    
    
    
    
    
    
}
