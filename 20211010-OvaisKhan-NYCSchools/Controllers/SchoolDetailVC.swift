//
//  SchoolDetailVC.swift
//  20211002-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/3/21.
//

import UIKit
import MapKit

class SchoolDetailVC: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = SchoolDetailVM()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("SAT SCORE DATA", self.viewModel.satScores)
        print("SCHOOL LIST DATA", self.viewModel.data)
        print("SCHOOL SCORE FOUND", self.viewModel.notFount)
        configUI()
    }
    

    func configUI(){
        
        self.navigationItem.title = self.viewModel.data?.school_name
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "ScoreCell", bundle: nil), forCellReuseIdentifier: "ScoreCell")
        tableView.register(UINib(nibName: "OverViewCell", bundle: nil), forCellReuseIdentifier: "OverViewCell")
        tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        tableView.register(UINib(nibName: "ExtraCurricularCell", bundle: nil), forCellReuseIdentifier: "ExtraCurricularCell")
    }
    
    
    @objc func openNavigation(_ sender: UIButton){
        let coordinate = CLLocationCoordinate2DMake(Double((self.viewModel.data?.latitude!)!)!, Double((self.viewModel.data?.longitude!)!)!)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "\(self.viewModel.data?.school_name ?? "School")"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }

}

extension SchoolDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreCell") as! ScoreCell
            cell.selectionStyle = .none
            if self.viewModel.notFount == true {
                cell.showEmptyListPlaceholder(title: "No SAT scores found for \(self.viewModel.data?.school_name ?? "")")
            } else {
                cell.hideEmptyListPlaceholder()
                cell.configureCell(scores: self.viewModel.satScores!)
            }
            
            return cell
        
        case 1 :
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OverViewCell") as! OverViewCell
            cell.selectionStyle = .none
            cell.configureCell(data: self.viewModel.data!)
            
        return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraCurricularCell") as!  ExtraCurricularCell
                cell.selectionStyle = .none
                cell.configureCell(data: self.viewModel.data!)
           
            return cell
        
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressCell
                cell.selectionStyle = .none
            cell.navigateBtn.addTarget(self, action: #selector(self.openNavigation(_:)), for: .touchUpInside)
                cell.configureCell(data: self.viewModel.data!)
                
            return cell
    default:
        break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


