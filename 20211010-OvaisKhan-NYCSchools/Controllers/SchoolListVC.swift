//
//  SchoolListVC.swift
//  20211002-OvaisKhan-NYCSchools
//
//  Created by Ovais Khan on 10/2/21.
//

import UIKit

class SchoolListVC: UIViewController, UISearchControllerDelegate {
    
    //Outlet
    @IBOutlet weak var tableView: BaseTableView!
    
    
    //ViewModel Decleration
    var viewModel = SchoolListVM()
    
    // Variables
    let searchBar = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesSearchBarWhenScrolling = false
        // Do any additional setup after loading the view.
        
        
        fetchData()
    }
    
    
    
    func setupSearch(){
        searchBar.searchResultsUpdater = self
        searchBar.searchBar.placeholder = "Search Schools"
        searchBar.obscuresBackgroundDuringPresentation = false
        searchBar.searchBar.tintColor = UIColor.black
        navigationItem.searchController = searchBar
        definesPresentationContext = true
        self.searchBar.delegate = self
        
    }
    
    
    func fetchData(){
        
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        self.viewModel.getSchoolList{ state in
            switch state {
            case .success:
                
                dispatchGroup.leave()
                
            case .failure:
                dispatchGroup.leave()
                print("API CALL FAILED")
            }
        }
        
        dispatchGroup.enter()
        self.viewModel.getScore(completion: { state in
            switch state {
            case .success:
                
                dispatchGroup.leave()
                
            case .failure:
                dispatchGroup.leave()
                
                print("API CALL FAILED")
            }
        })
        
        
        dispatchGroup.notify(queue: .main) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                
                
                self.setupSearch()
                self.configureTable()
                self.tableView.reloadData()
                
            }
        }
        
        
    }
    
    
    func configureTable(){
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.onRefreshHandler = fetchData
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            self.tableView.reloadData()
        }
        self.tableView.register(UINib(nibName: "ListTableCell", bundle: nil), forCellReuseIdentifier: "ListTableCell")
    }
    
    
    func filterSearchContent(_ searchText: String, scope: String = "All") {
        self.viewModel.filteredschools = (self.viewModel.schoolList?.filter({( schools : School_List_Model) -> Bool in
            return schools.school_name!.lowercased().contains(searchText.lowercased())
        }))!
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchBar.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchBar.searchBar.text?.isEmpty ?? true
    }
}


extension SchoolListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return self.viewModel.filteredschools.count
        }
        
        return self.viewModel.schoolList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath) as! ListTableCell
        cell.selectionStyle = .none
        
        if isFiltering() {
            
            let data = self.viewModel.filteredschools[indexPath.row]
            cell.configure(data: data)
            
        } else {
            if let data = self.viewModel.schoolList?[indexPath.row] {
                cell.configure(data: data)
                
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFiltering() {
            
            let data = self.viewModel.filteredschools[indexPath.row]
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailVC") as! SchoolDetailVC
                if  self.viewModel.satScores?.count != 0 {
                    
                    if let  index = self.viewModel.satScores?.firstIndex(where: { $0.dbn  == data.dbn }) {
                        self.viewModel.postSatScore = self.viewModel.satScores?[index]
                        vc.viewModel.data = self.viewModel.filteredschools[indexPath.row]
                        vc.viewModel.satScores = self.viewModel.postSatScore
                        vc.viewModel.notFount = false
                    } else {
                        vc.viewModel.data = self.viewModel.filteredschools[indexPath.row]
                        vc.viewModel.notFount = true
                    }
                    
                }
                self.navigationController?.pushViewController(vc, animated: true)
            
            
            
            
        } else {
           
            if let data = self.viewModel.schoolList?[indexPath.row] {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailVC") as! SchoolDetailVC
                if  self.viewModel.satScores?.count != 0 {
                    
                    if let  index = self.viewModel.satScores?.firstIndex(where: { $0.dbn  == data.dbn }) {
                        self.viewModel.postSatScore = self.viewModel.satScores?[index]
                        vc.viewModel.data = self.viewModel.schoolList?[indexPath.row]
                        vc.viewModel.satScores = self.viewModel.postSatScore
                        vc.viewModel.notFount = false
                    } else {
                        vc.viewModel.data = self.viewModel.schoolList?[indexPath.row]
                        vc.viewModel.notFount = true
                    }
                    
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
}


extension SchoolListVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterSearchContent(searchController.searchBar.text!)
    }
}
